import flask, wget, paramiko, threading
from flask import request, redirect, url_for
import config


app=flask.Flask(__name__)

downloading_files = {}
copying_files = {}

#wget_callback(current_size, total_size, width)

@app.route("/d", methods=["POST"])
def download():
    url = request.form['url']

    def download():
        def copy(filename):
            copying_files[url] = (0, 0, '')
            t = paramiko.Transport((config.SFTP_SERVER, config.SFTP_PORT))
            t.connect(None, config.SFTP_USER, config.SFTP_PASSWORD)
            sftp = paramiko.SFTPClient.from_transport(t)

            sftp = paramiko.SFTPClient.from_transport(t)
            def callback(current_size, total_size):
                copying_files[url] = (current_size, total_size, current_size * 1.0/ total_size * 100)
            sftp.put(filename, config.COPY_PREFIX + filename, callback)
            t.close()
            os.remove(filename)
            del copying_files[url]

        def callback(current_size, total_size, width):
            downloading_files[url] = (current_size, total_size, current_size * 1.0 / total_size * 100)
            if current_size == total_size:
                del downloading_files[url]

        downloading_files[url] = (0, 0, '')
        filename = wget.download(url, bar=callback)
        copy(filename)

    if url not in downloading_files.keys() + copying_files.keys():
        t = threading.Thread(target=download)
        t.start()

    return redirect(url_for('status'))

@app.route("/")
def status():
    return flask.render_template('status.html', downloading=downloading_files, copying=copying_files)

if __name__ == "__main__":
    app.run(host='0.0.0.0', port=5687, debug=True)
